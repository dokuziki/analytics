'use strict'

const Lucid = use("Lucid")

class Quota extends Lucid {
    static get table(){
        return 'quotas';
    }
    static get timestamps () {
        return false;
    }
    static get softDeletes () {
        return false
    }
}

module.exports = Quota
