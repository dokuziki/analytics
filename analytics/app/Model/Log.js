'use strict'
const mongoose = require('mongoose');
const db = use("App/Mongo");


var logSchema = new mongoose.Schema({
    from:  String,
    over:  String,
    to:  String,
    action:  String,
    appKey: String,
    data: Object,
    date: Date
});

module.exports = db.model('log', logSchema)
