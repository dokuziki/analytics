'use strict'

const Lucid = use("Lucid")

class Request extends Lucid {
    static get timestamps () {
        return false;
    }
}

module.exports = Request
