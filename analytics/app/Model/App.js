'use strict'

const Lucid = use("Lucid")

class App extends Lucid {

    static get softDeletes () {
        return 'deleted_at'
    }

    static get hidden () {
        return ['secretKey']
    }
}

module.exports = App
