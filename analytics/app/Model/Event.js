'use strict'

const Lucid = use("Lucid")

class Event extends Lucid {
    static get table(){
        return 'events';
    }
    static get timestamps () {
        return false;
    }
    static get softDeletes () {
        return false
    }
}

module.exports = Event;
