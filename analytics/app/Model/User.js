'use strict'
const mongoose = require('mongoose');
const db = use("App/Mongo");


var userSchema = new mongoose.Schema({
    token:  String,
    appKey: String,
    data: Object
});

module.exports = db.model('user', userSchema)
