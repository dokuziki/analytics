'use strict'
const mongoose = require('mongoose');
const db = use("App/Mongo");


var overSchema = new mongoose.Schema({
    type:  String,
    appKey:  String,
    token: String,
    data: Object
});

module.exports = db.model('over', overSchema)
