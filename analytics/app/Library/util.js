/**
 * Created by erenyildirim on 02/04/16.
 */

'use strict'

class Util {
    //Returning error messages only for debugging purposes
    static returnErrorResponse(message, response){
        return response.status(500).send({
            'status':false,
            'message':message
        });

    };
}

module.exports = Util;