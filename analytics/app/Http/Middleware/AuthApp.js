'use strict'

const AppModel = use('App/Model/App');
const RequestModel = use('App/Model/Request');
const CryptoJS = use("crypto-js");
const Env = use('Env');

class AuthApp {

    *handle(request, response, next) {

        const appKey = request.input('appKey');
        const timeStamp = request.input('timeStamp');
        const hashKey = request.input('hashKey');
        const ip = request.ip();
        

    /*
     *   Checking Hash and Timestamp Validation
     */
        var hashFlag = false;

        const appdetails = yield AppModel.where('appKey', appKey).first();


        
        if (appdetails != undefined)
            hashFlag = CryptoJS.SHA256(appdetails.secretKey + timeStamp).toString() == hashKey;
        else
            hashFlag = false;



    /*
     *   Checking request timestamp order
     */
        var requestFlag = false;

        const requestdetails = yield RequestModel.where('appKey', appKey).where('ip', ip).first();

        if (requestdetails != undefined) {

            if(requestdetails.last < timeStamp || requestdetails.last == null){

                let requestUpdate = yield RequestModel.find(requestdetails.id);
                requestUpdate.last = timeStamp;
                yield requestUpdate.update();

                requestFlag = true;
            }else{
                //TODO: ban user
                if(Env.get('DEBUG')){
                    console.log('banlandın ama şimdilik debug modundayız panpa sorun yok');
                    requestFlag = true;
                }else{
                    console.log('banlandın');
                    requestFlag = false;
                }
            }

        }else{
            yield RequestModel.create({last:timeStamp,appKey:appKey,ip:ip });
            requestFlag = true;
        }



        /*
         *   Request Flag and Hash Flag
         */
        if (/*hashFlag &&*/ requestFlag) {
           yield next
        }else{
            console.log('hashFlag: ',hashFlag);
            console.log('requestFlag: ',requestFlag);

            response.status(401).send({
                status:false,
                message:"Hash or Request is Not Valid ",
                hash:hashFlag,
                request:requestFlag,
                ip:ip
            });
            return
        }


    }


}


module.exports = AuthApp;
