'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Routes helps you in defining http endpoints/urls which will be used
| by outside world to interact with your application. Adonis has a
| lean and rich router to support various options out of the box.
|
*/
const Route = use('Route')

Route.any('/log', 'LogController.index').middlewares(['authapp','bancheck'])

Route.group('version1', function () {

    Route.get('quotas', 'ApiController.Quotas');
    Route.get('actions', 'ApiController.Actions');
    Route.get('total-count/:action', 'ApiController.Count');
    Route.get('user/:id', 'ApiController.User');

}).prefix('/api');

Route.get('/', 'HomeController.index')
