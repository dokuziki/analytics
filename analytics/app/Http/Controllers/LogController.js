'use strict'
const AppModel = use('App/Model/App');
const QuotaModel = use('App/Model/Quota');
const UserModel = use('App/Model/User');
const ToModel = use('App/Model/To');
const OverModel = use('App/Model/Over');
const LogModel = use('App/Model/Log');
const EventModel = use('App/Model/Event');
const Database = use('Database');

/*
const util = use('App/Library/Util');
*/

class LogController {

  * index (request, response) {

    const appKey = request.input('appKey');

    const action = request.input('action');
    const to = request.input('to');
    const over = request.input('over');

    const userObject = request.input('user');
    const toObject = request.input(to);
    const overObject = request.input(over);

    //var eventObject = yield EventModel.where('appKey', appKey).where('event-name', action).first();

    const eventObject = yield Database.from('events').where('appKey', appKey).where('event-name', action).first();


    if(eventObject == undefined || eventObject.amount == undefined){
      response.status(500).send({
        'status':false
      });
      //If there is no event defined return error or TODO: create event?
/*
      return util.returnErrorResponse('You have sent an undefined action name. Please define it first',response);
*/
    }

    var appDetails = {
      quota_type:"seller"
    };

    if(appDetails.quota_type == to){

          yield QuotaModel.where('appKey', appKey).where('object-type', to).where('object-id', toObject.id).decrement('quota',eventObject.amount);

    }

    let userMongo, toMongo, overMongo;

      let cond = {token: userObject.token, appKey:appKey};
      if(userObject.data.id !== undefined){
         cond = {token: userObject.token, appKey:appKey, "data.id":userObject.data.id};
      }

    UserModel.findOneAndUpdate(cond, {token:userObject.token,appKey:appKey, data:userObject.data}, {upsert: true, 'new': true}, function(err, res) {
      userMongo = res;

      ToModel.findOneAndUpdate({token: toObject.id,appKey:appKey,type:to}, {token:toObject.id,appKey:appKey,type:to,data:toObject }, {upsert: true, 'new': true}, function(err, res) {
        toMongo = res;

        OverModel.findOneAndUpdate({token: overObject.id,appKey:appKey,type:over}, {token:overObject.id,appKey:appKey,type:over,data:overObject }, {upsert: true, 'new': true}, function(err, res) {
          overMongo = res;

          var log = new LogModel({
            from: userMongo.token,
            to: toMongo.token,
            over: overMongo.token,
            action: action,
            appKey: appKey,
            date: new Date()
          });

          log.save(function (err,res) {
            //TODO: control
          });

        });

      });

    });



    response.status(200).send({
      'status':true
    });
    return
  }

}

module.exports = LogController;
