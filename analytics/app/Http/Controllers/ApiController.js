'use strict'
const AppModel = use('App/Model/App');
const QuotaModel = use('App/Model/Quota');
const UserModel = use('App/Model/User');
const ToModel = use('App/Model/To');
const LogModel = use('App/Model/Log');
const OverModel = use('App/Model/Over');
const appKey = 202401943278026;

class ApiController {

    * Quotas(request, response) {
        QuotaModel.orderBy('id','desc').fetch().then(_ => {
            let json = _.toJSON();
            let container = [];
            let recursiveFunc = i => {
                let obj = json[i++];
                ToModel.find({type: obj["object-type"], token: obj["object-id"]}).then(_ => {
                    container.push({title: _[0].data.title, logo: _[0].data.logo, quota: obj["quota"]});
                    if (i < json.length)
                        recursiveFunc(i);
                    else {
                        response.status(200).send({status: true, data: container})
                        return
                    }

                })
            };
            if (json.length > 0)
                recursiveFunc(0);
        });
    }

    * Actions(request, response) {
        LogModel.find({}).sort({"_id": -1}).limit(10).then(_ => {
            let json = _;
            let container = [];
            let recursiveFunc = i => {
                let obj = json[i++];
                let temp = {};

                UserModel.find({token: obj.from, appKey:appKey}).then(function(res) {
                    temp["from"] = {
                        pic:res[0].data.pic,
                        first_name:res[0].data.first_name,
                        last_name:res[0].data.last_name,
                        id:res[0].data.id,
                        token: res[0].token
                    }

                    ToModel.find({token: obj.to, appKey:appKey}).then(function(res) {
                        temp["to"] = {
                            title:res[0].data.title,
                            logo:res[0].data.logo
                        }

                        OverModel.find({token: obj.over, appKey:appKey}).then(function(res) {
                            temp["over"] = {
                                title:res[0].data.title,
                                img:res[0].data.img
                            }

                            temp["action"] = obj.action;

                            if (i < json.length){
                                container.push(temp);
                                recursiveFunc(i);
                            }
                            else {
                                response.status(200).send({status: true, data: container})
                                return
                            }

                        });
                    });

                });

            };
            if (json.length > 0)
                recursiveFunc(0);
        });
    }


    * Count(request, response) {
        let action = request.param('action');
        LogModel.find({action:action}).count(function(err, count) {
            response.status(200).send({status: true, data: count})
            return
        });

    }

    * User(request, response) {
        let id = request.param('id');

        console.log(id);
        UserModel.find({"data.id": id, appKey:appKey}).sort({"_id": -1}).then(function(res) {
            response.status(200).send({status: true, data: res})
            return
        });



    }

}

module.exports = ApiController;
