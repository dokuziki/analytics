'use strict'

const Schema = use('Schema')

class AppSchema extends Schema {

  up () {
    this.create('apps', function (table) {
      table.increments('id')
      table.string('appKey')
      table.string('secretKey')
      table.string('quota-enabled')
      table.timestamps()
      table.timestamp('deleted_at').nullable()
    })
  }

  down () {
    this.dropIfExists('apps')
  }

}

module.exports = AppSchema
