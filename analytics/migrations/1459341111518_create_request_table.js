'use strict'

const Schema = use('Schema')

class RequestSchema extends Schema {

  up () {
    this.create('requests', function (table) {
      table.increments('id')
      table.string('ip')
      table.string('appKey')
      table.bigInteger('last').unsigned()
    })
  }

  down () {
    this.dropIfExists('requests')
  }

}

module.exports = RequestSchema
