'use strict'

const Schema = use('Schema')

class EventSchema extends Schema {

  up () {
    this.create('events', function (table) {
      table.increments('id');
      table.string('event-name');
      table.string('amount');
      table.enum('type', ['increment', 'decrement']); // increment or decrement field
      table.string('appKey');
    })
  }

  down () {
    this.drop('events')
  }

}

module.exports = EventSchema
