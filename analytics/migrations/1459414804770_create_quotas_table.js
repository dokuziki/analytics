'use strict'

const Schema = use('Schema')

class QuotaSchema extends Schema {

  up () {
    this.create('quotas', function (table) {
      table.increments('id')
      table.string('object-id')
      table.string('object-type')
      table.string('appKey')
      table.integer('quota')
    })
  }

  down () {
    this.dropIfExists('quotas')
  }

}

module.exports = QuotaSchema;
